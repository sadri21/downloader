//
//  HistoryView.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 16/10/20.
//

import UIKit
import AVKit
import AVFoundation
import JGProgressHUD

class HistoryView: UIViewController {
    
    @IBOutlet weak var historyTableView: UITableView!
    var listFile = [FileSave]()
    let videoExtensionArr = ["mkv", "flv", "mp4", "wmv", "avi", "webm"]
    let audioExtensionArr = ["m4a", "aac", "mp3", "oog"]
    var fileName, folderName: String!
    var fileType: String!
    var progress = JGProgressHUD(style: .extraLight)
    var progressAnimation = JGProgressHUDFadeZoomAnimation()
    @IBOutlet weak var noHistorylabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        historyTableView.tableFooterView = UIView.init(frame: .zero)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        listFile.removeAll()
        for list in GlobalConfig().getAllFile() {
            listFile.append(list)
        }
        historyTableView.reloadData()
        if (listFile.isEmpty) {
            noHistorylabel.isHidden = false
        } else {
            noHistorylabel.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "playAudio") {
            let vc = segue.destination as! AudioPlayerView
            vc.fileName = fileName
        } else if (segue.identifier == "unzipFolderView") {
            let vc = segue.destination as! ZipFolderView
            vc.folderName = folderName
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HistoryView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as! HistoryCell
        cell.fileName.text = listFile[indexPath.row].fileName
        let extensionArr = listFile[indexPath.row].fileName.components(separatedBy: ".")
        let extensionString = extensionArr[extensionArr.count - 1]
        if (videoExtensionArr.contains(extensionString)) {
            cell.imageTypeFile.image = #imageLiteral(resourceName: "video")
        }
        if (audioExtensionArr.contains(extensionString)){
            cell.imageTypeFile.image = #imageLiteral(resourceName: "audio")
        }
        if (extensionString == "zip") {
            cell.imageTypeFile.image = #imageLiteral(resourceName: "folder")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        fileName = listFile[indexPath.row].fileName
        let extensionArr = listFile[indexPath.row].fileName.components(separatedBy: ".")
        let extensionString = extensionArr[extensionArr.count - 1]
        if (videoExtensionArr.contains(extensionString)) {
            let directory: URL = SystemDir.downloadDirectory
            let videoUrl = directory.appendingPathComponent(fileName)
            do {
                let videoExists : Bool = try videoUrl.checkResourceIsReachable()
                if videoExists {
                    let player = AVPlayer(url: videoUrl)
                    let playerController = AVPlayerViewController()
                    playerController.player = player
                    self.present(playerController, animated: true) {
                        playerController.player!.play()
                    }
                } else {
                    print("Video doesn't exist")
                }
            }
            catch let error as NSError {
                print(error)
            }
        }
        if (audioExtensionArr.contains(extensionString)){
            performSegue(withIdentifier: "playAudio", sender: nil)
        }
        if (extensionString == "zip") {
            progress.textLabel.text = "Please wait.."
            progress.detailTextLabel.text = "Decompresing file"
            progress.show(in: view)
            let fileManager = FileManager()
            var destinationURL = SystemDir.downloadDirectory
            let sourceUrl = destinationURL.appendingPathComponent(fileName)
            let folderNameArr = fileName.components(separatedBy: ".")
            folderName = folderNameArr.dropLast().joined(separator: ".")
            destinationURL.appendPathComponent(folderName)
            DispatchQueue.main.async {
                do {
                    try fileManager.createDirectory(at: destinationURL, withIntermediateDirectories: true, attributes: nil)
                    try fileManager.unzipItem(at: sourceUrl, to: destinationURL)
                    self.performSegue(withIdentifier: "unzipFolderView", sender: nil)
                    print("finish")
                    self.progress.dismiss()
                } catch {
                    print("Extraction of ZIP archive failed with error:\(error)")
                    self.performSegue(withIdentifier: "unzipFolderView", sender: nil)
                    self.progress.dismiss()
                }
            }
        }
    }
}
