//
//  DownloaderView.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 15/10/20.
//

import UIKit
import Moya
import RealmSwift
import AVKit
import AVFoundation
import ZIPFoundation
import JGProgressHUD

class DownloaderView: UIViewController {

    @IBOutlet weak var urlField: UITextField!
    @IBOutlet weak var progressDownload: UIProgressView!
    @IBOutlet weak var previewFile: UIView!
    @IBOutlet weak var heigtPreview: NSLayoutConstraint!
    @IBOutlet weak var fileName: UILabel!
    @IBOutlet weak var fileImage: UIImageView!
    var typeDowload, fileNameStr, folderName: String!
    var progress = JGProgressHUD(style: .extraLight)
    var progressAnimation = JGProgressHUDFadeZoomAnimation()
    @IBOutlet weak var pleaseWaitLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .light
        progress.animation = progressAnimation
        previewFile.layer.cornerRadius = 15
        previewFile.addCostumShadow()
        heigtPreview.constant = 0
        pleaseWaitLabel.isHidden = true
        previewFile.subviews.forEach {
            $0.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func playAction(_ sender: Any) {
        if (typeDowload == "video") {
            let directory = SystemDir.downloadDirectory
            let videoUrl = directory.appendingPathComponent(fileNameStr)
            do {
                let videoExists : Bool = try videoUrl.checkResourceIsReachable()
                if videoExists {
                    let player = AVPlayer(url: videoUrl)
                    let playerController = AVPlayerViewController()
                    playerController.player = player
                    self.present(playerController, animated: true) {
                        playerController.player!.play()
                    }
                } else {
                    print("Video doesn't exist")
                }
            }
            catch let error as NSError {
                print(error)
            }
        } else if (typeDowload == "audio") {
            performSegue(withIdentifier: "audioView", sender: nil)
        } else {
            progress.textLabel.text = "Please wait.."
            progress.detailTextLabel.text = "Decompresing file"
            progress.show(in: view)
            let fileManager = FileManager()
            var destinationURL = SystemDir.downloadDirectory
            let sourceUrl = destinationURL.appendingPathComponent(fileNameStr)
            let folderNameArr = fileNameStr.components(separatedBy: ".")
            folderName = folderNameArr.dropLast().joined(separator: ".")
            destinationURL.appendPathComponent(folderName)
            DispatchQueue.main.async {
                do {
                    try fileManager.createDirectory(at: destinationURL, withIntermediateDirectories: true, attributes: nil)
                    try fileManager.unzipItem(at: sourceUrl, to: destinationURL)
                    self.performSegue(withIdentifier: "unzipFolderView", sender: nil)
                    print("finish")
                    self.progress.dismiss()
                } catch {
                    print("Extraction of ZIP archive failed with error:\(error)")
                    self.performSegue(withIdentifier: "unzipFolderView", sender: nil)
                    self.progress.dismiss()
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "audioView") {
            let vc = segue.destination as! AudioPlayerView
            vc.fileName = fileNameStr
        } else if (segue.identifier == "unzipFolderView") {
            let vc = segue.destination as! ZipFolderView
            vc.folderName = folderName
        }
    }
    
    

    @IBAction func downloadAction(_ sender: Any) {
        urlField.resignFirstResponder()
        pleaseWaitLabel.isHidden = false
        pleaseWaitLabel.text = "Please wait..."
        UIView.animate(withDuration: 0.3) {
            self.heigtPreview.constant = 0
            self.previewFile.subviews.forEach {
                $0.isHidden = true
            }
            self.fileImage.image = nil
            self.view.layoutIfNeeded()
        }
       
        let fileNameArr = urlField.text?.components(separatedBy: "/")
        fileNameStr = fileNameArr![fileNameArr!.count - 1]
        let file = GlobalConfig().getAllFile().filter("fileName == '\(fileNameStr ?? "")'").first
        if (file == nil) {
            let extensionArr = fileNameStr.components(separatedBy: ".")
            print(fileNameStr!)
            let extensionString = extensionArr[extensionArr.count - 1]
            let videoExtensionArr = ["mkv", "flv", "mp4", "wmv", "avi", "webm"]
            let audioExtensionArr = ["m4a", "aac", "mp3", "oog"]
            
            if (videoExtensionArr.contains(extensionString) || audioExtensionArr.contains(extensionString) || extensionString  == "zip") {
                MoyaProvider<DownloadService>(plugins: [NetworkLoggerPlugin()]).request(.download(url: urlField.text!, fileName: fileNameStr)) { (progress) in
                    self.progressDownload.progress = Float(progress.progress)
                    print("progress : \(progress.progress)")
                } completion: { (result) in
                    switch result {
                    case .success(let response):
                        if (response.statusCode == 200) {
                            print("file download success")
                            let realm = try! Realm()
                            let file = FileSave()
                            file.fileName = self.fileNameStr
                            try! realm.write {
                                realm.add(file)
                            }
                            UIView.animate(withDuration: 0.3) {
                                self.heigtPreview.constant = 60
                                self.previewFile.subviews.forEach {
                                    $0.isHidden = false
                                }
                                self.fileName.text = self.fileNameStr
                                if (videoExtensionArr.contains(extensionString)) {
                                    self.fileImage.image = #imageLiteral(resourceName: "video")
                                    self.typeDowload = "video"
                                }
                                if (audioExtensionArr.contains(extensionString)) {
                                    self.fileImage.image = #imageLiteral(resourceName: "audio")
                                    self.typeDowload = "audio"
                                }
                                if (extensionString == "zip") {
                                    self.fileImage.image = #imageLiteral(resourceName: "audio")
                                    self.typeDowload = "zip"
                                }
                                self.view.layoutIfNeeded()
                                self.pleaseWaitLabel.text = "File has been download"
                            }
                        } else {
                            let alert = UIAlertController(title: nil, message: "Url not found" ,preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
                            self.present(alert, animated: true)
                        }
                    case .failure(let message):
                        let alert = UIAlertController(title: nil, message: message.errorDescription ,preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
                        self.present(alert, animated: true)
                    }
                }
            } else {
                let alert = UIAlertController(title: nil, message: "File not support" ,preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
                self.present(alert, animated: true)
                self.pleaseWaitLabel.isHidden = true
            }
        } else {
            let alert = UIAlertController(title: nil, message: "File already exist" ,preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
            self.present(alert, animated: true)
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIView {
    func addCostumShadow() {
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.4
    }
}


