//
//  SystemDir.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 15/10/20.
//

import Foundation

class SystemDir {
    static let documentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let filemanager = FileManager()
        return urls.first!
    }()
        
    static let cacheDirectory: URL = {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()

    static let downloadDirectory: URL = {
        let directory: URL = SystemDir.documentsDirectory.appendingPathComponent("FileDownloader/")
        return directory
    }()
}
