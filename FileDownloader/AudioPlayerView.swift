//
//  VideoPlayerView.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 16/10/20.
//

import UIKit
import AVFoundation
import AVKit

class AudioPlayerView: UIViewController {

    var audioPlayer = AVAudioPlayer()
    var toggleState = 1
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var playPauseButton: UIButton!
    var fileName: String!
    
    override func viewDidLoad() {
        title = fileName
        
        
        
        let directory = SystemDir.downloadDirectory
        
        let pathComponent = directory.appendingPathComponent(fileName)
        let filePath = pathComponent.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            print("FILE AVAILABLE")
        } else {
            print("FILE NOT AVAILABLE")
        }
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: URL(string: filePath)!)
            audioPlayer.prepareToPlay()
            audioPlayer.play()
        } catch {
            let alert = UIAlertController(title: nil, message: "Error file or file corrupt" ,preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
            self.present(alert, animated: true)
        }
        
        slider.maximumValue = Float(audioPlayer.duration)
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
       
//        let player = AVPlayer(url: )
//        let playerLayer = AVPlayerLayer(player: player)
//        playerLayer.frame = self.view.bounds
//        self.view.layer.addSublayer(playerLayer)
//        player.play()
    }
    
    @IBAction func playPauseButtonAction(_ sender: Any) {
        if toggleState == 1 {
            audioPlayer.play()
            toggleState = 2
            playPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        } else {
            audioPlayer.pause()
            toggleState = 1
            playPauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        audioPlayer.stop()
    }
    
    @IBAction func stopButtonAction(_ sender: Any) {
        audioPlayer.stop()
        audioPlayer.currentTime = 0
    }
    
    @objc func updateSlider() {
        slider.value = Float(audioPlayer.currentTime)
    }
    
    @IBAction func changeAudioTime(_ sender: Any) {
        audioPlayer.stop()
        audioPlayer.currentTime = TimeInterval(slider.value)
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
