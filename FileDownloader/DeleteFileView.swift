//
//  DeleteFileView.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 16/10/20.
//

import UIKit
import RealmSwift

class DeleteFileView: UIViewController {

    @IBOutlet weak var deleteDataButton: UIButton!
    let fileManager = FileManager.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func deleteAction(_ sender: Any) {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        let files = try! fileManager.contentsOfDirectory(at: SystemDir.downloadDirectory, includingPropertiesForKeys: nil)
        for file in files {
            try! fileManager.removeItem(at: file)
        }
        let alert = UIAlertController(title: nil, message: "All data has been delete" ,preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: nil))
        self.present(alert, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
