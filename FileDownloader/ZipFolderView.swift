//
//  ZipFolderView.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 16/10/20.
//

import UIKit

class ZipFolderView: UIViewController {

    @IBOutlet weak var fileTable: UITableView!
    var listFile = [URL]()
    var folderName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let fileManager = FileManager.default;
        title = folderName
        fileTable.tableFooterView = UIView.init(frame: .zero)
        listFile = try! fileManager.contentsOfDirectory(at: SystemDir.downloadDirectory.appendingPathComponent(folderName), includingPropertiesForKeys: nil)
        fileTable.reloadData()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ZipFolderView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listFile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fileCell") as! HistoryCell
        cell.fileName.text = "\(listFile[indexPath.row].lastPathComponent)"
        return cell
    }
    
    
}
