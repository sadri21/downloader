//
//  HistoryCell.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 16/10/20.
//

import UIKit

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var imageTypeFile: UIImageView!
    @IBOutlet weak var fileName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
