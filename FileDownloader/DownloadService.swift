//
//  DownloadService.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 15/10/20.
//

import Foundation
import Moya
import SwiftyJSON


enum DownloadService {
    case download(url: String, fileName: String?)
    
    var localLocation: URL {
            switch self {
            case .download(_, let fileName):
                let fileKey = fileName ?? "" // use url's md5 as local file name
                let directory: URL = SystemDir.downloadDirectory
                let filePath: URL = directory.appendingPathComponent(fileKey)
                print(filePath)
                return filePath
            }
        }
    
    var downloadDestination: DownloadDestination {
            // `createIntermediateDirectories` will create directories in file path
            return { _, _ in return (self.localLocation, [.removePreviousFile, .createIntermediateDirectories]) }
        }
}

extension DownloadService: TargetType {
    var baseURL: URL {
        switch self {
        case .download(let url, _):
            return URL(string: url)!
        }
    }
    
    var path: String {
        switch self {
        case .download(_, _):
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .download(_, _):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .download(url: _, fileName: _):
            return .downloadParameters(parameters: ["":""], encoding: URLEncoding.default, destination: downloadDestination)
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    
}

