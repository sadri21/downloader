//
//  GlobalConfig.swift
//  FileDownloader
//
//  Created by Humancloudz Mobile on 16/10/20.
//

import Foundation
import RealmSwift

class GlobalConfig {
    func getFile() -> FileSave {
        let realm = try! Realm()
        let file = realm.objects(FileSave.self).first
        return file!
    }
    
    func getAllFile() -> Results<FileSave> {
        let realm = try! Realm()
        let file = realm.objects(FileSave.self)
        return file
    }
    
}
